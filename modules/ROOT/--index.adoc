= Welcome to the Orbis _{page-component-version}_!
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -

//This is the automatic start page for Orbis version *{page-component-version}*.
== What is Orbis?
Orbis is a direct-to-consumer platform for managing access to video content such 
as immersive sports events. 
Orbis handles all of the technical details using a single interface, from content 
curation and storage, from scaling the infrastructure to 
ISP outreach and reputation monitoring to whitelist services and real time analytics.

Using Orbis, content providers can create solutions 
that allow fans to discover and watch live video on web, mobile, and other 
connected devices. Orbis works hand-in-hand with the Aventus media processing 
platform to deliver a consistently high-quality viewing experience to the fan. 

.With Orbis, your fans can easily: 
* Purchase access to live or on-demand video content.
* Watch content over the web or through a downloaded client app.
* Find related VOD or upcoming content personalized to their interests.

Orbis provides reliable and scalable content, billing, and account management 
to your application so that you can focus on building the ideal UI and video 
appliation for your target audience.

== Authentication and Authorization Requirements
Before you can make Orbis API requests as a developer, you must register and configure 
your access to all third-party services that Orbis depends on. 
Some of these services (such as Vindicia and ContentWise) require the appropriate API 
key as part of the service request.

////
== Who is Orbis for?
Orbis is for anyone that needs to send email, whether it’s transactional email 
or marketing emails and campaigns. We send billions of emails each month, so we 
can handle your email regardless of volume.

Developers
For developers, it’s super easy to get started. To send email through SendGrid 
from your code, just change your email configuration to point to our servers and 
include the credentials for your SendGrid account, and you’re done. If you’re not 
already sending email from your app, we have a variety of integration options for you.

Once you’ve got email sending squared away, check out some of the other awesome things 
you can do with our APIs and apps.

Marketing Campaigns
Our Marketing Campaigns feature lets you compose and send email campaigns. We provide 
list management, delivery scheduling, A/B split testing, email analytics, and dynamic 
list segmentation. You can take a look at our Marketing Campaigns documentation to 
learn more.

We also provide a robust Marketing Campaigns API if you want to integrate any of these 
features into your own application.
////

////
Orbis is one of two documentation components in the iStreamPlanet Antora Docs Demo.
The other docs component, Aventus, can be accessed via the component selector menu 
(aka component drawer) at the bottom of the menu on the left side of the page.

== Page source

This page is sourced from the AsciiDoc file named [.path]_index.adoc_ that is located 
at [.path]_workspace/orbis/modules/ROOT/pages_.

=== Why is this the home page of Orbis?

This page is automatically used as the start page of Orbis because it is stored in the ROOT 
module and named [.path]_index.adoc_.
////


////
=== Why is this the home page of the Antora Demo site?

This page is used as the home page for the Antora Demo because it is assigned as the site `start_page` in the Demo site's playbook file [.path]_site.yml_.

== Cross reference syntax to target this page

To create a cross reference *to this page from another page in the ROOT module of Orbis*, the xref syntax would be `\xref:index.adoc[]`.

To create a cross reference *to this page from a page in Module One of Orbis*, the xref syntax would be `\xref:ROOT:index.adoc[]`.

=== Always target the latest version of this page

To create a cross reference *to the latest version of this page from a page in Aventus*, the xref syntax would be `\xref:component-b::index.adoc[]`.

=== Target a specific version of this page

To create a cross reference *to version 1.0 of this page from a page in Aventus*, the xref syntax would be `\xref:1.0@component-b::index.adoc[]`.
////