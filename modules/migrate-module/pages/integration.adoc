= Integration Guide
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -


////

== Admonition styles

An admonition, also known as a notice, helps draw attention to a line or block of text with a special label or icon.

Asciidoctor comes with five built-in styles.  Here is their suggested use:

* NOTE - Useful information for a specific context. (often used)
* TIP - Nice to know, but not required information. (occasionally used)
* IMPORTANT - Take this action or understand this concept before proceeding! (occasionally used)
* CAUTION - Data loss or corruption may result.  Minor or moderate injury may result if not heeded. (seldom used)
* WARNING - Serious physical injury or death may result if not heeded. (rarely used)

== Basic admonition

.Basic admonitions
[source,asciidoc]
----
CAUTION: Don't stick forks in electric sockets.

NOTE: After someone sticks a fork in a socket, you'll need to reset the circuit in the breaker box in the dark and scary basement.

WARNING: Never go into the basement.

IMPORTANT: A monster lives in the basement.

TIP: If you go into the basement, see if you can find Kenny's orange parka.
----

CAUTION: Don't stick forks in electric sockets.

TIP: After someone sticks a fork in a socket, you'll need to reset the circuit in the breaker box in the dark and scary basement.

WARNING: Never go into the basement.

IMPORTANT: A monster lives in the basement.

NOTE: If you go into the basement, see if you can find Kenny's orange parka.

== Complex admonition

.Example block masquerading as an admonition
[source,asciidoc]
----
[IMPORTANT]
.Optional Title
====
Use an example block to create an admonition that contains complex content, such as (but not limited to):

* Lists
* Multiple paragraphs
* Source code
* Images
====
----

[IMPORTANT]
.Optional Title
====
Use an example block to create an admonition that contains complex content, such as (but not limited to):

* Lists
* Multiple paragraphs
* Source code
* Images
====

////
