= Managing Billing Plans and Products
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -

// Playbook = How to guide for DTV admin and curator roles

== Conceptual Description

== Workflow Diagram

== Scenarios
. First
. Second
. Third



////
An admonition, also known as a notice, helps draw attention to a line or block of text with a special label or icon.

// .image:orbis.svg[] 
// * xref:index.adoc[image:orbis.svg[] What's New]
// .icon:heart[size=2x] ???

[%interactive]
* [ ] 1. Binge watch Death in Paradise
* [ ] 2. Visit a tropical island
* [ ] 3. Live on a boat


Is there a way to number checkbox items?

// [%interactive]
* [ ] 1. Binge watch Death in Paradise
* [ ] 2. Visit a tropical island


Asciidoctor comes with five built-in styles.

* NOTE
* TIP
* IMPORTANT
* CAUTION
* WARNING

== Basic admonition

.Basic admonitions
[source,asciidoc]
----
CAUTION: Don't stick forks in electric sockets.

TIP: After someone sticks a fork in a socket, you'll need to reset the circuit in the breaker box in the dark and scary basement.

WARNING: Never go into the basement.

IMPORTANT: A monster lives in the basement.

NOTE: If you go into the basement, see if you can find Kenny's orange parka.
----

CAUTION: Don't stick forks in electric sockets.

TIP: After someone sticks a fork in a socket, you'll need to reset the circuit in the breaker box in the dark and scary basement.

WARNING: Never go into the basement.

IMPORTANT: A monster lives in the basement.

NOTE: If you go into the basement, see if you can find Kenny's orange parka.

== Complex admonition

.Example block masquerading as an admonition
[source,asciidoc]
----
[IMPORTANT]
.Optional Title
====
Use an example block to create an admonition that contains complex content, such as (but not limited to):

* Lists
* Multiple paragraphs
* Source code
* Images
====
----

[IMPORTANT]
.Optional Title
====
Use an example block to create an admonition that contains complex content, such as (but not limited to):

* Lists
* Multiple paragraphs
* Source code
* Images
====
////

