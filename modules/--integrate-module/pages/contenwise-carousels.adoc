= Carousel Creation via ContentWise
// !!! ====================================================================================================
// TODO This entire section on Carousels needs to be replaced with our iSP solution.
// *!* Also see: https://istreamplanet.atlassian.net/wiki/spaces/OD/pages/604636780/Orbis+Carousel+Curation
// TODO Duplicate this, remove all references to ContentWise, add API Use Cases, add any Portal SOPs...
// TODO Create new diagrams describing how our carousels work.
// !!! ====================================================================================================

ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:experimental:

:iSP: iStreamPlanet
//:includedir: _includes
:sourcedir: ../integrate-module/ROOT/pages
:source-highlighter: pygments
// The valid options are coderay, highlightjs, prettify, and pygments.

== Overview and Onboarding
You can use ContentWise to create and customize carousels for video on 
demand, program channels, and live content. 
Your application calls the desired landing page definition through the {iSP} 
platform. 
As outlined in the following diagram, ContentWise uses a layered structure 
to provide flexible carousel functionality.  

// This is a stop gap measure until a simpler process is available.  
// Carlos's videos: https://drive.google.com/drive/folders/18K0dSzyKjGLLExmZ8neCVbj-3zMDA4tm


////
== Terminology

// include::{sourcedir}/doc-glossary.adoc[tag=carousel]
// <<doc-glossary.adoc#item,items>> 

Block:: 
A block refers to an ordered list of items that are shown to the user. 

Editorial List:: 
The {iSP} platform currently supports three types of editorial lists: +

* _cw.video_ for Video on Demand content
* _cw.channels_ for stations
* _cw.programs_ for EPG schedules

Use Case:: 
set of contents selected according to a configured strategy.

Page:: 
A page refers to a lander experience that is shown to the user of the 
OTT app - the high level navigation component where content is arranged in a 
defined layout. A page consists of one or more sections.

// include::{sourcedir}/doc-glossary.adoc[tag=carousel]
// <<doc-glossary.adoc#item,items>> 

Section:: 
A section refers to an ordered list of items that are shown to the user. 

Target:: 
Major geographic location where you provide service, usually a country or region.  

UX Reference:: 
A section refers to an ordered list of items that are shown to the user. 

Item:: 
An item is a specific piece of content that is associated with video content - 
either a movie or an episode or a show. This is the basic unit of content that 
a user can interact with. 
////

image:CW-Overview.png[details screen, role="text-left"]
// image::CW-Overview.png[details screen, role="text-left", width="80%", scaledwidth="75%"]

Call your {iSP} Customer Experience Mananager (CEM) to set up your ContentWise accounts. 

For the onboarding process, you must provide:

* [ ] A range of valid IP addresses for your installation.
* [ ] A list of email addresses with corresponding initial passwords.
* [ ] A list of your primary geographic service locations (countries or regions).

// == Initial Configuration

{iSP} will use the provided information during initial configuration
to set up your geographic targets and multi-region landing pages. 
If you find that you need addtional targets or landing pages, 
contact your {iSP} CEM.  

IMPORTANT: ContentWise provides many options for generating and configuring media carousels. 
Following the instructions listed here will successfully generate carousels for this platform. 
Consult the ContentWise documentation for detailed descriptions of the available options.
// However, closely adhering to the instructions provided here will help ensure your success.


////
'''
.Temporary diagram with callouts
[source]
----
data BinTree: // <1>
  | leaf
  | node(value, left, right)
end

fun tree-sum(t): // <2>
  doc: "Calculate the sum of node values"
  cases (BinTree) t:
    | leaf => 0
    | node(v, l, r) =>
      v + tree-sum(l) + tree-sum(r)
  end
where: // <3>
  tree-sum(leaf) is 0
  node4 = node(4, leaf, leaf)
  tree-sum(node(5, node4, leaf)) is 9
end
----
<1> A data structure
<2> A recursive function
<3> Three tests for the function
'''
////



== Curating Editorial Lists

This procecure describes how to manually add editorial list content for 
specific use cases.

NOTE: When logging in, the ContentWise portal enforces geographic and IP 
address restrictions as a security measure. 
You must conform to your original location and IP address range or 
use an appropriate VPN when logging in.

As indicated in the following diagram, when defining new carousels be sure to create or configure the components 
in the following order: 
&#x278A; Editorial Lists, 
&#x278B; Use Cases, 
&#x278C; Page Blocks.

image:CW-Configure.png[details screen, role="text-left"]

//image::CW-Create.png[details screen, role="text-left", width="80%", scaledwidth="75%"]


=== &#x278A; Creating Editorial Lists

. Select the menu:UX_Design[] tab at the top of the page. 
. Select the menu:UX_Builder[Editorial Lists] menu. +
The ContentWise portal displays a number of editorial list placeholders.
. Under the Actions column, select the edit icon btn:[&#x270E;] for the 
desired placeholder, such as _CuratedList_. 
. Within the Add content section, enter any text that you seek within 
the Search field then press the kbd:[Enter] key. +
The curated list displays any existing content for the selected item.
. Optionally, select a target language with which to filter your results.
. Optionally, select the *Advanced options* link to filter the results by type 
of content or to change the number of results displayed in the list.
. Select the Info icon ( &#9432; )  
to view additional details about any desired item.
. Select the btn:[+] icon to add any desired items to the current editorial list.
. Select the btn:[delete] icon to remove any desired items from the editorial list.
. Optionally, select the btn:[+ Add all] button to add all of the items to the 
editorial list.

The editorial list is now available in _Use Cases: Layout_.

'''

=== &#x278B; Configuring Live Content Carousels 
This procedure describes how to configure a carousel for live programs that are
currently playing.
//Editing a Layout Use Case

. Select the menu:UX_Design[] tab at the top of the page. 
. Select the menu:UX_Builder[Use Cases: Layout] menu. +
Optionally, enter any text that you seek within the Filter by Name field to 
filter the list of use cases.
. Under the Actions column, select the edit icon btn:[&#x270E;] for the 
desired layout use case. 
. Set or update the text in the Name and Description fields.
. Select the *CW.PROGRAMS* content type from the Subdomain field to display 
EPG schedules for live content.
+
// the following options are also available for 
// * _CW.VIDEO_ for video on demand content
// * _CW.CHANNELS_ for stations

. Choose whether to enable _Item Awareness_.
// Member Of field has been skipped for now.
. Optionally, enter a name for the _Layout UI Title_.
. On the Layout tab, add the number of items that you want to appear in the carousel.
// TODO: EPC: This is deeeeeeep! 
. To change the specific setting for any element:
.. Select the element.
.. Select the *Type* from the drop-down list.
.. Select the specific *Editorial list* that you want to appear for this element. +
Optionally, enter text in this field to filter the list of items you are searching.
.. Choose a *Selection Policy*.
// EPC: Ignoring "Custom Slot Properties.
.. Select the btn:[Apply] button.

'''

=== &#x278C; Configuring Carousel Pages and Blocks

Creating a New Page::
This procedure describes how to set up a landing page and an initial block structure.

. Select the menu:UX_Design[] tab at the top of the page. 
. Select the menu:UX_Builder[Pages] menu.
. Select the btn:[+ New] button.
. Enter a *Name* for the new landing page.
. Optionally, enter a *Description* for the page.
. Select the set of desired *Targets* from the list of geographic targets.
. Within the Structure area:
.. Select and configure each available *Current Target*.
.. For each block within the Target Layout:
... Select the btn:[+ Add Block] button.
... Select the edit button btn:[&#x270E;] to configure the block.
.... Select *Layout* for the Use Case Type.
//.... Select the *Use Case Type*: either Layout, Search, or Continue Watching.
.... Select the desired Use Case. Optionally, you can enter any text that you seek 
to filter your choices for this field. 
.... Optionally, enter a *Block Label* to access this specific block later.
.... Within the Presentation area, choose whether to enable the *Auto Load*, *Auto Slide*, 
*Infinite Scrolling*, and *User Can Scroll* features. 
.... Choose either *Horizontal* or *Vertical* for the Thumbnail Orientation.
... Select the btn:[Apply] button.
.. If needed, arrange the blocks in the desired order. 
.. Select the btn:[Save] button.

The new landing page is now available for selection.

'''

Updating Carousel Pages and Blocks::
This procedure describes how to modify the block structure for a landing page.

. Select the menu:UX_Design[] tab at the top of the page. 
. Select the menu:UX_Builder[Pages] menu.
. Under the Actions column, select the edit icon btn:[&#x270E;] for the 
landing page that you wish to modify, such as _HOME_, _LIVE_, or _SPORTS_. 
. Optionally, update the Name and Description for the page.
. Optionally, update the set of geographic *Targets* for the page.
. Within the Structure area:
.. Optionally select and reconfigure each available *Current Target*.
.. For each block within the Target Layout:
... Optionally, select the block title to rename it.
... Select the btn:[+ Add Block] button to add additional blocks.
... Select the edit button btn:[&#x270E;] to reconfigure an existing block.
.... Select *Layout* for the Use Case Type.
//.... Select the *Use Case Type*: either Layout, Search, or Continue Watching.
.... Select the desired Use Case. Optionally, you can enter any text that you seek 
to filter your choices for this field. 
.... Optionally, enter a *Block Label* to access this specific block later.
.... Within the Presentation area, choose whether to enable the *Auto Load*, *Auto Slide*, 
*Infinite Scrolling*, and *User Can Scroll* features. 
.... Choose either *Horizontal* or *Vertical* for the Thumbnail Orientation.
... Select the btn:[Apply] button.
.. Optionally, rearrange the blocks to suit your purposes.
.. Select the btn:[Save] button.

The updated landing page is now available for selection.


//== Adding an Existing Carousel to a New Page
// You may want to display the same (or a similar) carousel on multiple pages.
// (What is a likely scenario for this?)


'''
'''


////
// =========================================================================== 

== Using OCM APIs to manage content curation
Orbis OCM APIs are currently catered to create pages and sections within the 
pages that are manually curated (where assets are handpicked). 
This section describes how you can use OCM APIs to curate content.

=== Create Pages and Sections

[cols="^6,15,15,24,40"]
|===
|Order	
|Step	
|Prerequisite	
|API	
|Notes

|1	
|Create a page and associate sections with the page	
|None	
|https://documenter.getpostman.com/view/3590248/RVfwgpnj#be780d91-85da-4276-a490-d08e0cb426b0[PUT Create/Update page]
|This is the very first step when creating a page. 
Make sure that the pages are created with the right 
sections inside them before adding content to sections. +
 +
Please note that when you do a CREATE PAGE on an existing page, 
it will overwrite the existing page contents.

|2	
|Create sections in the page	
|Create page and associate sections	
|https://documenter.getpostman.com/view/3590248/RVfwgpnj#72f42ab1-c97b-4f0c-9128-4498a2793941[PUT Create/Update section]
|Ensure that the pageID and sectionID belong in the create section call. 

|3	
|Verify that the page and sections have been created 	
|Create page and associate sections, create section.	
|https://documenter.getpostman.com/view/3590248/RVfwgpnj#760c52e4-1bef-484e-b981-1d88a9340e9b[GET Get page] +
https://documenter.getpostman.com/view/3590248/RVfwgpnj#747ac7b5-f0e2-46d3-ae5a-9ad3f644b9eb[GET Get a section of a page]
|Use the get APIs on pages and sections to validate that the pages and sections are created
|===

==== Example

.1. First, create a page with a carousel in it. Associate the sections at this point.
// .example 1.json
// [#ex1-json] 
[source,json,coderay]
----
'PUT {{OCMServer}}/ocm/v2/pages/config'

// Body     // 
{
  "pageID": "CARTEST",
  "sections":["CARTEST01"]
}

// Response // 
{
    "requestID": "887fcdb4-e96d-415c-891b-9d962c0ebe70",
    "timestamp": "2018-06-06T17:33:22.050224614Z",
    "page": {
        "pageID": "CARTEST",
        "sections": [
            "CARTEST01"
        ]
    }
}
----
// <1> This is a test of the callout feature
// <2> This is only a test


.2. Next, create the section and add items to the section.
// .example 2.json
// [#ex2-json] 
[source,json]
----
PUT {{OCMServer}}/ocm/v2/pages/CARTEST/sections/config

//Body
{
  "sectionID": "CARTEST01",
  "assetIDs": ["313068e28d1dae6b5fe326f70231b0a9", "07830a713a808bc3e4e5afa34334c6e3"]
}

//Response
{
    "requestID": "02d56528-5625-483c-8117-7e6efeab74f0",
    "timestamp": "2018-06-06T23:31:09.335028643Z",
    "section": {
        "sectionID": "CARTEST01",
        "assetIDs": [
            "313068e28d1dae6b5fe326f70231b0a9",
            "07830a713a808bc3e4e5afa34334c6e3"
        ]
    }
}
----


.3. Lastly, validate it by getting the contents of the page or the section.
// .example 3.json
// [#ex3-json] 
[source,json]
----
GET {{OCMServer}}/ocm/v2/pages/CARTEST?region=Default

//Response
{
    "requestID": "b3a05a98-3866-41c7-b3e1-3785cda564ff",
    "timestamp": "2018-06-06T23:33:09.846800957Z",
    "page": {
        "sections": [
            {
                "sectionID": "CARTEST01",
                "subsections": [
                    {
                        "items": [
                            {
                                "position": 1,
                                "id": "313068e28d1dae6b5fe326f70231b0a9",
                                "resourceType": "unknown"
                            },
                            {
                                "position": 2,
                                "id": "07830a713a808bc3e4e5afa34334c6e3",
                                "resourceType": "unknown"
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
----

=== Update the page with a new section

[cols="6,15,15,24,40"]
|===
|Order	
|Step	
|Prerequisite	
|API	
|Notes

|1	
|Create the same page and associate sections with the page	
|None	
|https://documenter.getpostman.com/view/3590248/RVfwgpnj#be780d91-85da-4276-a490-d08e0cb426b0[PUT Create/Update page]
|Ensure that you make a create call with the same pageID as the existing page. 
At the same time, ensure that you define the content of the whole page. DO NOT 
focus on the update itself.

|2	
|Create sections in the page	
|Create page and associate sections	
|https://documenter.getpostman.com/view/3590248/RVfwgpnj#72f42ab1-c97b-4f0c-9128-4498a2793941[PUT Create/Update section]
|

|3	
|Verify that the page and sections have been created 	
|Create page and associate sections, create section.	
|https://documenter.getpostman.com/view/3590248/RVfwgpnj#760c52e4-1bef-484e-b981-1d88a9340e9b[GET Get page] +
https://documenter.getpostman.com/view/3590248/RVfwgpnj#747ac7b5-f0e2-46d3-ae5a-9ad3f644b9eb[GET Get a section of a page]
|
|===


=== Update a section with new items

[cols="6,15,15,24,40"]
|===
|Order	
|Step	
|Prerequisite	
|API
|Notes

|1	
|Create sections in the page	
|Create page and associate sections.	
|https://documenter.getpostman.com/view/3590248/RVfwgpnj#72f42ab1-c97b-4f0c-9128-4498a2793941[PUT Create/Update section]
|Ensure that the pageID and sectionID belong in the create section call.  
At the same time, ensure that you define the content of the whole section. 
DO NOT focus on the update itself. 

|2	
|Verify that the page and sections have been created 	
|Create page and associate sections, create section.	
|https://documenter.getpostman.com/view/3590248/RVfwgpnj#760c52e4-1bef-484e-b981-1d88a9340e9b[GET Get page] +
https://documenter.getpostman.com/view/3590248/RVfwgpnj#747ac7b5-f0e2-46d3-ae5a-9ad3f644b9eb[GET Get a section of a page]
|
|===

////
